<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{    
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        return view('penduduk.index', compact('posts'));
    }
    
    public function create()
    {
        return view('penduduk.create');
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama'      => 'required',
            'nik'       => 'required|max:16',
            'tgl_lahir' => 'required',
            'tmp_lahir' => 'required',
            'photo'     => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'alamat'    => 'required'
        ]);

        $photo = $request->file('photo');
        $photo->storeAs('public/image', $photo->hashName());

        Post::create([
            'nama'     => $request->nama,
            'nik'   => $request->nik,
            'tgl_lahir' => $request->tgl_lahir,
            'tmp_lahir' => $request->tmp_lahir,
            'photo'     => $photo->hashName(),
            'alamat' => $request->alamat,
        ]);
        return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function edit(Post $post)
    {
        return view('penduduk.edit', compact('post'));
    }

    public function update(Request $request, Post $post){

        // $this->validate($request, [
        //     'nama'      => 'required',
        //     'nik'       => 'required|max:16',
        //     'tgl_lahir' => 'required',
        //     'tmp_lahir' => 'required',
        //     'photo'     => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     'alamat'    => 'required'
        // ]);

        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $photo->storeAs('public/image', $photo->hashName());
            Storage::delete('public/image/'.$post->photo);
            $post->update([
                'nama'      => $request->nama,
                'nik'       => $request->nik,
                'tgl_lahir' => $request->tgl_lahir,
                'tmp_lahir' => $request->tmp_lahir,
                'photo'     => $photo->hashName(),
                'alamat'     => $request->alamat,
            ]);
        }else{
            $post->update([
                'nama'     => $request->nama,
                'nik'   => $request->nik,
                'tgl_lahir' => $request->tgl_lahir,
                'tmp_lahir' => $request->tmp_lahir,
                'alamat' => $request->alamat,
            ]);
        }
        return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Diubah!']);

    }
    public function destroy(Post $post)
    {
        Storage::delete('public/posts/'. $post->image);
        $post->delete();
        return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}