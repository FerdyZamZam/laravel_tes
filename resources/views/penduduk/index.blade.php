@extends('layouts/app')
@section('dashboard', 'active')
@section('content')

<div class="container-fluid">
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>

    <div class="row clearfix">
        <div class="col-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Data Penduduk
                    </h2>
                </div>

                <br>

                <div class="container-fluid">
                    <div class="pull-right">
                        <a href="{{ route('posts.create') }}" class="btn btn-md btn-success waves-effect">TAMBAH</a>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Photo</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Nik</th>
                                <th scope="col">Tanggal Lahir</th>
                                <th scope="col">Tempat Lahir</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($posts as $post)
                            <tr>
                                <td class="text-center">
                                    <img src="{{ Storage::url('public/image/').$post->photo }}" class="rounded" style="width: 150px">
                                </td>
                                <td>{{ $post->nama }}</td>
                                <td>{{ $post->nik }}</td>
                                <td>{{ $post->tgl_lahir }}</td>
                                <td>{{ $post->tmp_lahir }}</td>
                                <td>{!! $post->alamat !!}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-danger">
                                Data Post belum Tersedia.
                            </div>
                            @endforelse
                        </tbody>
                    </table>  
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script>
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>
@endsection