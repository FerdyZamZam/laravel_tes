@extends('layouts/app')
@section('dashboard', 'active')
@section('content')

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit Data Penduduk
                    </h2>
                </div>

                <br>


                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-12">
                       
                            <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Photo</h2>
                                        <input type="file" class="form-control" name="photo">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Nama</h2>
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama', $post->nama) }}" placeholder="Masukkan Nama">
                            
                            @error('nama')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Nik</h2>
                                        <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik', $post->nik) }}" placeholder="Masukkan Nik">
                            
                                @error('nik')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Tempat Lahir</h2>
                                        <input type="text" class="form-control @error('tmp_lahir') is-invalid @enderror" name="tmp_lahir" value="{{ old('tmp_lahir', $post->tmp_lahir) }}" placeholder="Masukkan tempat lahir">
                                        
                                        @error('tmp_lahir')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Tanggal Lahir</h2>
                                        <input type="date" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="{{ old('tgl_lahir', $post->tgl_lahir) }}" placeholder="Masukkan tanggal lahir">
                                    
                                    @error('tgl_lahir')
                                        <div class="alert alert-danger mt-2">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <h2 class="card-inside-title">Alamat</h2>
                                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" rows="5" placeholder="Masukkan Alamat">{{ old('alamat', $post->alamat) }}</textarea>
                                @error('alamat')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-md btn-primary">UPDATE</button>
                            <button type="reset" class="btn btn-md btn-warning">RESET</button>

                            </form>
                        </div>
                    </div>
               
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection